# Ingest service

Manage tcpdump captures on the vulnbox. Capture for user-configurable intervals
and periodically extract conversations with tcpflow and sync with s3.

## S3 data format:

    cap_<epoch_start_time>/
        <epoch_start_time>.pcap
        tcpflow_file1.flow
        tcpflow_file2.flow
        %T_%a-%b_%A-%B (tcpflow_file3.flow)

    cap_1520808593/
    ├── 2018-03-11T22:50:03Z_00443-49440_149.020.004.015-192.168.174.130.flow
    ├── 2018-03-11T22:50:03Z_49438-00443_192.168.174.130-149.020.004.015.flow
    ├── 2018-03-11T22:50:03Z_49440-00443_192.168.174.130-149.020.004.015.flow
    ├── 2018-03-11T22:50:03Z_49442-00443_192.168.174.130-149.020.004.015.flow
    ├── 2018-03-11T22:50:04Z_00443-49442_149.020.004.015-192.168.174.130.flow
    ├── 2018-03-11T22:50:06Z_00080-57706_092.242.140.021-192.168.174.130.flow
    ├── cap_1520808593.pcap
    └── report.xml
	cap_1520808613/
	cap_1520808633/
	...

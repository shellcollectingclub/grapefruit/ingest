#!/bin/bash

#
# Install and start the ingest service
#
# This script will install dependencies and interactively prompt for
# AWS s3 credentials for awscli.
#

# Install dependencies
sudo apt install tcpflow
sudo -H pip3 install --upgrade pip
sudo -H pip3 install -r requirements.txt

# Interactively set up awscli
sudo aws configure

# Set up ingest service
sudo cp ingest/ingest.py /usr/local/bin/
sudo cp ingest/ingest.yaml /etc/
sudo mkdir -p /pcaps/

sudo cp ingest/ingest.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable ingest
sudo systemctl start ingest

echo
echo "  Ingest service installed!"
echo "    Binary: /usr/local/bin/ingest.py"
echo "    Config: /etc/ingest.yaml"
echo "    Logs:   /var/log/ingest.log"
echo "    Pcaps:  /pcaps/"
echo
echo "  Run 'service ingest status' for current status"
echo

#!/usr/bin/env python3

import datetime
import logging
import os
import sched
import subprocess
import time

import dateutil.parser
import pytimeparse
import pytz
import tzlocal
import yaml

logging.basicConfig(level=logging.DEBUG,
                    format="[%(asctime)s] %(levelname)s: %(message)s",
                    filename="/var/log/ingest.log",
                    datefmt="%Y-%m-%d %H:%M:%S")
logger = logging.getLogger(__name__)

config = None


class Config(object):
    """Container class for parsed configuration options."""

    CONFFILE_PATHS = [ "ingest.yaml", "/etc/ingest.yaml" ]

    def __init__(self):
        conffile = None
        for path in Config.CONFFILE_PATHS:
            if os.path.isfile(path):
                conffile = path
        if conffile is None:
            raise RuntimeError("Could not find configuration file 'ingest.yaml'")

        with open(conffile, "r") as f:
            yml = yaml.load(f)

        self.capture_start = dateutil.parser.parse(yml["capture"]["start"])
        self.capture_end = dateutil.parser.parse(yml["capture"]["end"])
        self.capture_interval = pytimeparse.parse(yml["capture"]["interval"])
        self.round_interval = pytimeparse.parse(yml["round"]["interval"])
        self.host = yml["vulnbox"]["host"]
        self.port = int(yml["vulnbox"]["port"])
        self.user = yml["vulnbox"]["user"]
        self.password = yml["vulnbox"].get("password")           # can be None
        self.identity_file = yml["vulnbox"].get("identity_file") # can be None
        self.s3_bucket = yml["s3"]["bucket"]

        if self.password is None and self.identity_file is None:
            raise RuntimeError("Must specify vulnbox password or identity file")

        if self.password is not None:
            os.environ["SSHPASS"] = self.password


def run_vulnbox_cmd(command):
    """Run a remote command on the vulnbox.

    :param command: Shell command to run.
    :return: The output from the command.
    """
    if config.password:
        cmd = "sshpass -e ssh -oStrictHostKeyChecking=no -p {:d} {:s}@{:s} '{:s}'"
        cmd = cmd.format(config.port, config.user, config.host, command)
    else:
        cmd = "ssh -oStrictHostKeyChecking=no -i {:s} -p {:d} {:s}@{:s} '{:s}'"
        cmd = cmd.format(config.identity_file, config.port, config.user,
                         config.host, command)
    logger.debug(cmd)
    return os.system(cmd)


def pull_vulnbox_file(rpath, lpath):
    """Pull a file from the vulnbox.

    :param rpath: Remote path for the file to pull.
    :param lpath: Local path to store the file.
    """
    if config.password:
        cmd = "sshpass -e scp -o StrictHostKeyChecking=no -P {:d} {:s}@{:s}:{:s} {:s}"
        cmd = cmd.format(config.port, config.user, config.host, rpath, lpath)
    else:
        cmd = "scp -o StrictHostKeyChecking=no -i {:s} -P {:d} {:s}@{:s}:{:s} {:s}"
        cmd = cmd.format(config.identity_file, config.port, config.user,
                         config.host, rpath, lpath)
    logger.debug(cmd)
    return os.system(cmd)


def start_capture():
    """Start a tcpdump capture. Return the name of the capture file on the
        remote box."""
    now = int(time.time())
    pcap = "{:d}.pcap".format(now)
    out = "{:d}.out".format(now)
    cmd = "screen -S capture_{0:s} -dm tcpdump -w pcaps/{0:s} not port 22".format(pcap)
    run_vulnbox_cmd(cmd)
    return pcap


def stop_capture(pcap):
    cmd = "screen -S capture_{:s} -X quit".format(pcap)
    run_vulnbox_cmd(cmd)
    time.sleep(5)


def sync_s3(path):
    logger.info("Syncing %s to s3://%s", path, config.s3_bucket)
    cmd = "aws s3 sync {:s} s3://{:s}".format(path, config.s3_bucket)
    os.system(cmd)
    logger.info("S3 sync complete")


def get_next_tick(now):
    """Calculate the amount of seconds until the next tick.

    :param now: datetime object for the current time.
    """
    if now < config.capture_start:
        return (config.capture_start - now).total_seconds()

    # delta - num of seconds since start
    # next_round_delta - num of seconds since start till next round
    # next_round - num of seconds until next round
    delta = (now - config.capture_start).total_seconds()
    next_round_delta = ((delta // config.round_interval) + 1) * config.round_interval
    next_round = next_round_delta - delta
    return min(next_round, config.capture_interval)


def get_round_no(epoch):
    """Calculate the round number for a given epoch time."""
    tz = tzlocal.get_localzone()
    tm = datetime.datetime.fromtimestamp(float(epoch))
    tm = tz.localize(tm)

    delta = int((tm - config.capture_start).total_seconds())
    return delta // config.round_interval + 1


def tick(scheduler, last_pcap=None):
    """Actions to take at every scheduled interval.

    This function will start a new capture and schedule the next tick before
    processing the capture from the previous round. This ensures no gaps in
    captures. Previous captures will be pulled back and synced to s3. Following
    the initial sync, conversations will be extracted and synced to s3 as well.

    :param scheduler: sched.scheduler object.
    :param last_pcap: Name of the previous capture file on the vulnbox.
    """
    # Schedule the next tick and start a new capture
    now = pytz.utc.localize(datetime.datetime.utcnow())

    if now < config.capture_end:
        new_pcap = start_capture()
        wait = get_next_tick(now)
        scheduler.enter(wait, 1, tick, (scheduler, new_pcap))
        logger.info("Started new capture: %s", new_pcap)
        logger.info("Scheduled next tick for %d seconds", wait)
    else:
        logger.info("Competition ended! Did not start a new capture.")

    if last_pcap is not None:
        # Stop and pull old capture
        logger.info("Stopping old capture: %s", last_pcap)
        stop_capture(last_pcap)
        name, _ = os.path.splitext(last_pcap)
        capdir = "/pcaps/round{:d}/cap_{:s}/".format(get_round_no(name), name)
        os.makedirs(capdir)
        pull_vulnbox_file("pcaps/" + last_pcap, capdir + last_pcap)
        run_vulnbox_cmd("rm pcaps/" + last_pcap)
        logger.info("Downloaded capture: %s", capdir + last_pcap)

        # Extract conversations from the latest pcap and sync with s3
        sync_s3("/pcaps/")
        logger.info("Extracting conversations...")
        cmd = "tcpflow -r {:s} -T %T_%a-%b_%A-%B.flow".format(last_pcap)
        subprocess.call(cmd, cwd=capdir, shell=True)
        sync_s3("/pcaps/")
    else:
        logger.info("No previous capture detected")


def run(config):
    """Continuously start and stop captures, and ingest the pcap data."""
    s = sched.scheduler(time.time, time.sleep)
    now = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)

    if now < config.capture_start:
        # The competition hasn't started. Wait for the start time.
        wait = get_next_tick(now)
        s.enter(wait, 1, tick, (s,))
        logger.info("Competition will start in %d seconds", wait)
    else:
        # The competition already started and the service is restarting
        # Start a new capture immediately
        logger.info("Competition already started - starting capture now")
        tick(s)

    s.run()


def main():
    global config
    config = Config()
    run(config)


if __name__ == "__main__":
    main()

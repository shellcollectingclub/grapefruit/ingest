#!/bin/bash

#
# Setup script for the vulnbox.
#
# This script will add a user to capture traffic and allow that user
# to run tcpdump without sudo access.
#

sudo apt-get install tcpdump

# Create user in the pcap group for capturing. PCAPs will be stored in this
# user's home directory.
sudo adduser tcpdump
sudo groupadd pcap
sudo usermod -a -G pcap tcpdump

# Allow users in the pcap group to run tcpdump without root
sudo chgrp pcap /usr/sbin/tcpdump
sudo chmod 750 /usr/sbin/tcpdump
sudo setcap CAP_NET_RAW,CAP_NET_ADMIN+eip /usr/sbin/tcpdump
